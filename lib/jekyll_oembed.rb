require 'oembed'

# Register all default OEmbed providers
::OEmbed::Providers.register_all
# Since register_all does not register all default providers,
# we need to do this here.
# See https://github.com/judofyr/ruby-oembed/issues/18
::OEmbed::Providers.register(
  ::OEmbed::Providers::Instagram,
  ::OEmbed::Providers::Slideshare,
  ::OEmbed::Providers::Yfrog,
  ::OEmbed::Providers::MlgTv
)
::OEmbed::Providers.register_fallback(
  ::OEmbed::ProviderDiscovery,
  ::OEmbed::Providers::Noembed
)

module Jekyll
  module Filters
    module OEmbed
      # Gets the embedding HTML or returns an iframe
      def oembed(url)
        return '' unless url

        if (o = fetch(url))
          o.html
        else
          "<iframe src=\"#{url}\"></iframe>"
        end
      end

      # Gets the thumbnail URL
      #
      # TODO: DRY
      def oembed_thumbnail(url)
        return '' unless url

        if (o = fetch(url))
          o.thumbnail_url
        else
          ''
        end
      end

      private

      # Fetch and cache the OEmbed information
      def fetch(url)
        begin
          cache.getset(url) do
            ::OEmbed::Providers.get(url)
          end
        rescue
          Jekyll.logger.warn "OEmbedError: #{url} is not available as an oembed."
        end
      end

      def cache
        @cache ||= Jekyll::Cache.new('Jekyll::Filters::OEmbed')
      end
    end
  end
end

Liquid::Template.register_filter(Jekyll::Filters::OEmbed)
