# jekyll-oembed

oEmbed plugin for jekyll that creates an `oembed` liquid filter.

This is an incompatible fork of
[jekyll-oembed](https://github.com/18F/jekyll-oembed)

## Installation

### With bundler

Within your project's `Gemfile`, add the following within the
`:jekyll_plugins` group:

```ruby
# Gemfile
group :jekyll_plugins do
  gem 'jekyll_oembed',
    git: 'https://0xacab.org:sutty/jekyll/jekyll-oembed.git'
end
```

## Usage

To embed an URL, pass it through the `oembed` filter:

```liquid
  {{ 'https://www.youtube.com/watch?v=oRvX0abNWvg' | oembed }}
  {{ page.video_url | oembed }}
```

`jekyll_oembed` does not support customizing width, height, or adding
any attributes directly to the embedded HTML.

To get the thumbnail URL for videos, use:

```liquid
<img src="{{ 'https://www.youtube.com/watch?v=oRvX0abNWvg' | oembed_thumbnail }}"/>
```


## Limitations

Protected URLs: some URLs are private. If this is the case, oembed _may_
not function properly.


## Attribution

Thank you to:

- @vanto for the [plugin code](https://gist.github.com/vanto/1455726)
  that powers the gem
- @stereobooster for creating a [different
  version](https://github.com/stereobooster/jekyll_oembed) of the gem
- @18F for the [source plugin](https://github.com/18F/jekyll-oembed)

## Resources

- [oEmbed providers](http://www.oembed.com/#section7.1)
- [`ruby-oembed`](https://github.com/ruby-oembed/ruby-oembed)


## Contributing
1. Fork it
2. Create your feature branch (`git checkout -b my-new-feature`)
3. Commit your changes (`git commit -am 'Added some feature'`)
4. Push to the branch (`git push origin my-new-feature`)
5. Create new Pull Request
